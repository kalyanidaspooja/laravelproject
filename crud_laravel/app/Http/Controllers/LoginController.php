<?php
  
namespace App\Http\Controllers;
 
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
 
class LoginController extends Controller
{

 

    function checklog(Request $request)
    {
     // $this->validate($request, [
     //  'email'   => 'required|email',
     //  'password'  => 'required|alphaNum|min:3'
     // ]);

     $user_data = array(
      'email'  => $request->get('email'),
      'password' => $request->get('password')
     );

     if(Auth::attempt($user_data))
     {
       //print_r($user_data['email']);
       //die();
     $request->session()->put('name',$user_data['email']);

      return redirect('/users');
     }
     else
     {
      return back()->with('error', 'Wrong Login Details');
     }

    }

  
    function logout()
    {
     Auth::logout();
     return redirect('/login');
    }
}
